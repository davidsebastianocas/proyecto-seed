/**
 * ---------------------------------------------------------------------
 * $Id: ListaS.java,v 2.0 2013/08/23
 * Universidad Francisco de Paula Santander
 * Programa Ingenieria de Sistemas
 *
 * Proyecto: SEED_UFPS
 * ----------------------------------------------------------------------
 */
package ufps.util.colecciones_seed;

import java.util.Collections;
import java.util.Iterator;

/**
 * Implementacion de la Clase Lista Simple para el manejo de Listas Encadenadas.
 *
 * @param <T> Tipo de datos a almacenar en la lista.
 * @author Marco Adarme
 * @version 2.0
 */
public class ListaS<T> implements Iterable<T> {

    ////////////////////////////////////////////////////////////
    // ListaS - Atributos //////////////////////////////////////
    ////////////////////////////////////////////////////////////
    /**
     * Representea el Nodo cabecera de la Lista
     */
    private Nodo<T> cabeza;

    /**
     * Representa el tamaño de la Lista
     */
    private int tamaño;

    ////////////////////////////////////////////////////////////
    // ListaS - Implementacion de Metodos //////////////////////
    ////////////////////////////////////////////////////////////
    /**
     * Constructor de la Clase Lista Simple Enlazada, por defecto la cabeza es
     * NULL. <br>
     * <b>post: </b> Se construyo una lista vacia.
     */
    public ListaS() {
        this.cabeza = null;
        this.tamaño = 0;
    }

    /**
     * Metodo que inserta un Elemento al Inicio de la Lista. <br>
     * <b>post: </b> Se inserto un nuevo elemento al inicio de la Lista.<br>
     *
     * @param x Informacion que desea almacenar en la Lista. La informacion debe
     * ser un Objeto.
     */
    public void insertarAlInicio(T x) {
        this.cabeza = new Nodo<T>(x, this.cabeza);
        this.tamaño++;
    }

    /**
     * Metodo que inserta un Elemento al Final de la Lista. <br>
     * <b>post: </b> Se inserto un nuevo elemento al final de la Lista.<br>
     *
     * @param x Información que desea almacenar en la Lista.
     */
    public void insertarAlFinal(T x) {
        if (this.cabeza == null) {
            this.insertarAlInicio(x);
        } else {
            try {
                Nodo<T> ult = this.getPos(this.tamaño - 1);
                if (ult == null) {
                    return;
                }
                ult.setSig(new Nodo<T>(x, null));
                this.tamaño++;
            } catch (ExceptionUFPS e) {
                System.err.println(e.getMensaje());
            }
        }
    }

    /**
     * Metodo que inserta un Elemento de manera Ordenada desde la cabeza de la
     * Lista. <br>
     * <b>post: </b> Se inserto un nuevo elemento en la posicion segun el Orden
     * de la Lista.<br>
     *
     * @param info Información que desea almacenar en la Lista de manera
     * Ordenada.
     */
    public void insertarOrdenado(T info) {
        if (this.esVacia()) {
            this.insertarAlInicio(info);
        } else {
            Nodo<T> x = this.cabeza;
            Nodo<T> y = x;
            while (x != null) {
                Comparable comparador = (Comparable) info;
                int rta = comparador.compareTo(x.getInfo());
                if (rta < 0) {
                    break;
                }
                y = x;
                x = x.getSig();
            }
            if (x == y) {
                this.insertarAlInicio(info);
            } else {
                y.setSig(new Nodo<T>(info, x));
                this.tamaño++;
            }
        }
    }

    /**
     * Metodo que elimina un elemento dada una posición. <br>
     * <b>post: </b> Se elimino el dato en la posicion de la lista indicada.<br>
     *
     * @param i Una posición en la Lista <br>
     * @return El elemento que elimino. Si la posición no es válida retorna
     * NULL.
     */
    public T eliminar(int i) {
        if (this.esVacia()) {
            return null;
        }
        Nodo<T> t = this.cabeza;
        if (i == 0) {
            this.cabeza = this.cabeza.getSig();
        } else {
            try {
                Nodo<T> y = this.getPos(i - 1);
                t = y.getSig();
                y.setSig(t.getSig());
            } catch (ExceptionUFPS e) {
                System.err.println(e.getMensaje());
                return (null);
            }// 1 2 3 4 5
        }
        t.setSig(null);
        this.tamaño--;
        return (t.getInfo());
    }

    /**
     * Metodo que elimina todos los datos de la Lista Simple. <br>
     * <b>post:</b> La Lista Simple se encuentra vacia.
     */
    public void vaciar() {
        this.cabeza = null;
        this.tamaño = 0;
    }

    /**
     * Metodo que retorna el elemento que se encuentre en una posicion dada.
     * <br>
     * <b>post: </b> Se retorno el elemento indicado por la posicion
     * recibida.<br>
     *
     * @param i Una Posición dentro de la Lista. <br>
     * @return El objeto que se encuentra en esa posición. El objeto <br>
     * retorna su valor parametrizada "T". Si la posición no se <br>
     * encuentra en la Lista retorna null.
     */
    public T get(int i) {
        try {
            Nodo<T> t = this.getPos(i);
            return (t.getInfo());
        } catch (ExceptionUFPS e) {
            System.err.println(e.getMensaje());
            return (null);
        }

    }

    /**
     * Metodo que edita el elemento que se encuentre en una posición dada. <br>
     * <b>post: </b> Se edito la informacion del elemento indicado por la
     * posicion recibida.<br>
     *
     * @param i Una Posición dentro de la Lista. <br>
     * @param dato es el nuevo valor que toma el elmento en la lista
     */
    public void set(int i, T dato) {
        try {
            Nodo<T> t = this.getPos(i);
            t.setInfo(dato);
        } catch (ExceptionUFPS e) {
            System.err.println(e.getMensaje());
        }
    }

    /**
     * Metodo que obtiene la cantidad de elementos de la Lista. <br>
     * <b>post: </b> Se retorno el numero de elementos existentes en la
     * Lista.<br>
     *
     * @return int con el tamaño de la lista. Si la Lista esta vacía retorna 0
     */
    public int getTamaño() {
        return (this.tamaño);
    }

    /**
     * Metodo que verifica si la Lista esta o no vacia. <br>
     * <b>post: </b> Se retorno true si la lista se encuentra vacia, false si
     * tiene elementos.<br>
     *
     * @return true si la lista esta vacia , false si contiene elementos.
     */
    public boolean esVacia() {
        return (this.cabeza == null);
    }

    /**
     * Metodo que busca un elemento en la lista si lo encuentra retorna true, de
     * lo contrario false. <br>
     * <b>post: </b> Se retorno true si se encontro el elementos buscado, false
     * si no fue asi.<br>
     *
     * @param info el cual contiene el valor del parametro a buscar en la lista.
     * <br>
     * @return un boolean, si es true encontro el dato en la lista y si es false
     * no lo encontro.
     */
    public boolean esta(T info) {
        return (this.getIndice(info) != -1);
    }

    /**
     * Metodo que crea para la lista simple un elemento Iterator.
     * <b>post: </b> Se retorno un Iterator para la Lista.<br>
     *
     * @return Un iterator tipo <T> de la lista.
     */
    @Override
    public Iterator<T> iterator() {
        return new IteratorLS<T>(this.cabeza) {
        };
    }

    /**
     * Metodo que permite retornar la informacion de una Lista en un Vector.
     * <br>
     *
     * @return Un vector de Objetos con la informacion de cada posicion de la
     * Lista.
     */
    public Object[] aVector() {
        if (this.esVacia()) {
            return (null);
        }
        Object vector[] = new Object[this.getTamaño()];
        Iterator<T> it = this.iterator();
        int i = 0;
        while (it.hasNext()) {
            vector[i++] = it.next();
        }
        return (vector);
    }

    /**
     * Metodo que retorna toda la informacion de los elementos en un String de
     * la Lista. <br>
     * <b>post: </b> Se retorno la representacion en String de la Lista. El
     * String tiene el formato "e1->e2->e3..->en", donde e1, e2, ..., en son los
     * los elementos de la Lista. <br>
     *
     * @return Un String con los datos de los elementos de la Lista
     */
    @Override
    public String toString() {
        if (this.esVacia()) {
            return ("Lista Vacia");
        }
        String r = "";
        for (Nodo<T> x = this.cabeza; x != null; x = x.getSig()) {
            r += x.getInfo().toString() + "\n";
        }
        return (r);
    }

    /**
     * Metodo privado de la clase que devuelve al elemento en la posicion. <br>
     * <b>post: </b> Se retorno el Nodo que se encuentra en esa posicion
     * indicada. <br>
     *
     * @param i Una posici�n en la Lista. <br>
     * @return El elemento encontrado. Si la posici�n no es v�lida retorna null
     */
    private Nodo<T> getPos(int i) throws ExceptionUFPS {
        if (this.esVacia() || i > this.tamaño || i < 0) {
            throw new ExceptionUFPS("El índice solicitado no existe en la Lista Simple");
        }
        Nodo<T> t = this.cabeza;
        while (i > 0) {
            t = t.getSig();
            i--;
        }
        return (t);
    }

    /**
     * Metodo que obtiene la posición de un objeto en la Lista. <br>
     * <b>post: </b> Se retorno la posicion en la que se encuentra el dato
     * buscado.
     *
     * @param info Objeto que se desea buscar en la Lista <br>
     * @return un int con la posición del elemento,-1 si el elemento no se
     * encuentra en la Lista
     */
    public int getIndice(T info) {
        int i = 0;
        for (Nodo<T> x = this.cabeza; x != null; x = x.getSig()) {
            if (x.getInfo().equals(info)) {
                return (i);
            }
            i++;
        }
        return (-1);
    }

    public void ordenarInsercionPorInfos() throws ExceptionUFPS {
        if (this.esVacia() || this.tamaño == 1) {
            throw new ExceptionUFPS("No es posible ordenar la lista");
        }

        Nodo<T> aux1 = this.cabeza;
        Nodo<T> aux2 = aux1.getSig();

        while (aux2 != null) {
            if (comparar(aux1.getInfo(), aux1.getSig().getInfo()) > 0) {
                T camb = aux1.getInfo();
                aux1.setInfo(aux1.getSig().getInfo());
                aux1.getSig().setInfo(camb);

                if (aux1 != this.cabeza) {
                    aux1 = getAnterior(aux1);
                }
            } else {
                aux2 = aux2.getSig();
                aux1 = getAnterior(aux2);
            }
        }
    }

    public void ordenarInsercionPorNodos() throws ExceptionUFPS {
        if (this.esVacia() || this.tamaño == 1) {
            throw new ExceptionUFPS("No es posible ordenar la lista");
        }
        int cont = 0;
        Nodo<T> aux1 = this.cabeza;
        Nodo<T> aux2 = null;

        while (aux1 != null) {
            Nodo<T> auxSig = aux1.getSig();

            if (aux2 == null || comparar(aux2.getInfo(), aux1.getInfo()) > 0) {
                aux1.setSig(aux2);
                aux2 = aux1;
            } else {
                Nodo<T> auxOrd = aux2;

                while (auxOrd.getSig() != null && comparar(auxOrd.getSig().getInfo(), aux1.getInfo()) < 0) {
                    auxOrd = auxOrd.getSig();
                }

                aux1.setSig(auxOrd.getSig());
                auxOrd.setSig(aux1);
            }
            aux1 = auxSig;
            cont++;
            if (cont == this.tamaño / 3) {
                System.out.println("He we voy por el 33%");
            }
            if (cont == this.tamaño / 2) {
                System.out.println("He we voy por el 50%");
            }
            if (cont == (this.tamaño / 3) * 2) {
                System.out.println("He we voy por el 66%");
            }
        }
        this.cabeza = aux2;
    }

    public int comparar(T info1, T info2) {
 
        Comparable comparable = (Comparable) info1;
        return comparable.compareTo(info2);
    }

    public Nodo<T> getAnterior(Nodo<T> aux) throws ExceptionUFPS{

        if(this.esVacia() || aux == this.cabeza || !esta(aux.getInfo())){
            throw new ExceptionUFPS("No es posible obtener el nodo anterior");
        }
        
        Nodo<T> temp = this.cabeza;
        while (temp.getSig() != aux) {
            temp = temp.getSig();
        }

        return temp;
    }

    public boolean contieneRepetidos(ListaS l2)throws ExceptionUFPS {
        if (this.esVacia() || l2.esVacia()) {
            throw new ExceptionUFPS("Una de las listas esta vacia");
        }
        
        Nodo<T> auxl1 = this.cabeza;
        Nodo<T> auxl2 = l2.cabeza;
        while (auxl1 != null) {
            while (auxl2 != null) {
                if (comparar(auxl1.getInfo(), auxl2.getInfo()) == 0) {
                    System.out.println(auxl1.getInfo());
                    return true;

                }
                auxl2 = auxl2.getSig();
            }
            auxl1 = auxl1.getSig();
            auxl2 = l2.cabeza;
        }
        return false;
    }

    public boolean noContieneRepetidos(ListaS l2) throws ExceptionUFPS{
        if (this.esVacia() || l2.esVacia()) {
            throw new ExceptionUFPS("Una de las listas esta vacia");
        }
        
        Nodo<T> auxl1 = this.cabeza;
        Nodo<T> auxl2 = l2.cabeza;
        while (auxl1 != null) {
            while (auxl2 != null) {
                if (comparar(auxl1.getInfo(), auxl2.getInfo()) == 0) {
                    System.out.println(auxl1.getInfo());
                    return false;

                }
                auxl2 = auxl2.getSig();
            }
            auxl1 = auxl1.getSig();
            auxl2 = l2.cabeza;
        }
        return true;
    }

    public void eliminarElementoRepetido(ListaS l2) throws ExceptionUFPS{
        if (this.esVacia() || l2.esVacia()) {
            throw new ExceptionUFPS("Una de las listas esta vacia");
        }

        Nodo<T> aux = this.cabeza;
        while (aux != null) {

            if (l2.esta(aux.getInfo())) {
                this.eliminar(getIndice(aux.getInfo()));
                aux = this.cabeza;
            } else {
                aux = aux.getSig();
            }
        }
    }

    public boolean sonIguales(ListaS l2) throws ExceptionUFPS{
        if (this.esVacia() || l2.esVacia()) {
            throw new ExceptionUFPS("Una de las listas esta vacia");
        }

        if (this.tamaño != l2.getTamaño()) {
            return false;
        }

        Nodo<T> auxl1 = this.cabeza;
        Nodo<T> auxl2 = l2.cabeza;
        while (auxl1 != null || auxl2 != null) {

            if (comparar(auxl1.getInfo(), auxl2.getInfo()) != 0) {
                return false;
            } else {

                auxl1 = auxl1.getSig();
                auxl2 = auxl2.getSig();
            }
        }
        return true;
    }

    public ListaS combinarListaS(ListaS l2) throws ExceptionUFPS {
        if (l2.esVacia()) {
            throw new ExceptionUFPS("La lista 2 esta vacia");
        }

        ListaS l3 = new ListaS();
        Nodo<T> nodAux = this.getPos(tamaño - 1);
        nodAux.setSig(l2.cabeza);
        this.tamaño += l2.getTamaño();

        Nodo<T> aux = this.cabeza;
        while (aux != null) {
            if (l3.esta(aux.getInfo()) == false) {
                l3.insertarAlInicio(aux.getInfo());
            }
            aux = aux.getSig();
        }
        return l3;
    }

    public ListaS combinarListaSAscendente(ListaS l2) throws ExceptionUFPS {
        if (l2.esVacia()) {
            throw new ExceptionUFPS("La lista 2 esta vacia");
        }
        ListaS l4 = new ListaS();
        Nodo<T> nodAux = this.getPos(tamaño - 1);
        nodAux.setSig(l2.cabeza);
        this.tamaño += l2.getTamaño();

        Nodo<T> aux = this.cabeza;
        while (aux != null) {
            l4.insertarOrdenado(aux.getInfo());
            aux = aux.getSig();
        }
        return l4;
    }

    public void quickSort(Object x[], int auxIzq, int auxDer)throws ExceptionUFPS {// Si usar, huele a limon
        if (x.length == 0) {
            throw new ExceptionUFPS("La lista 2 esta vacia");
        }
        Object t;
        int k = auxIzq;
        int b = auxDer;
        Object pivote;
        if (auxDer > auxIzq) {
            pivote = x[(auxIzq + auxDer) / 2];
            while (k < b) {
                while ((k < auxDer) && (comparar((T) x[k], (T) pivote) < 0)) {
                    ++k;
                }
                while ((b > auxIzq) && (comparar((T) x[b], (T) pivote) > 0)) {
                    --b;
                }
                if (k <= b) {
                    t = x[k];
                    x[k] = x[b];
                    x[b] = t;
                    ++k;
                    --b;
                }
            }
            if (auxIzq < b) {
                quickSort(x, auxIzq, b);
            }
            if (k < auxDer) {
                quickSort(x, k, auxDer);
            }
        }
    }

    public void quickSort2(int auxIzq, int auxDer) throws ExceptionUFPS {//no usar, huele a ovo

        Object x[] = this.aVector();
        Object t;
        int k = auxIzq;
        int b = auxDer;
        Object pivote;
        if (auxDer > auxIzq) {
            pivote = x[(auxIzq + auxDer) / 2];
            while (k < b) {
                while ((k < auxDer) && (comparar((T) x[k], (T) pivote) < 0)) {
                    ++k;
                }
                while ((b > auxIzq) && (comparar((T) x[b], (T) pivote) > 0)) {
                    --b;
                }
                if (k <= b) {
                    t = x[k];
                    x[k] = x[b];
                    x[b] = t;
                    ++k;
                    --b;
                }
            }
            if (auxIzq < b) {
                this.quickSort2(auxIzq, b);
            }
            if (k < auxDer) {
                this.quickSort2(k, auxDer);
            }
        }
        this.vaciar();
        for (int i = 0; i < x.length; i++) {
            this.insertarAlFinal((T) x[i]);
        }

    }

    public void invertirLista() throws ExceptionUFPS {

        Nodo<T> auxInicio = this.cabeza;
        Nodo<T> auxFinal = this.getPos(tamaño - 1);
        int pivote = tamaño / 2;
        while (getIndice(auxInicio.getInfo()) < pivote) {

            T aux = auxInicio.getInfo();
            auxInicio.setInfo(auxFinal.getInfo());
            auxFinal.setInfo(aux);

            auxInicio = auxInicio.getSig();
            auxFinal = getAnterior(auxFinal);
        }
    }

    public ListaS interseccionDeListas(ListaS l2) throws ExceptionUFPS {

        ListaS interseccion = new ListaS();
        Nodo<T> auxl1 = this.cabeza;
        Nodo<T> auxl2 = l2.cabeza;

        while (auxl1 != null) {

            while (auxl2 != null) {

                if (comparar(auxl1.getInfo(), auxl2.getInfo()) == 0 && interseccion.esta(auxl2.getInfo()) != true) {
                    interseccion.insertarAlInicio(auxl1.getInfo());
                }
                auxl2 = auxl2.getSig();
            }
            auxl1 = auxl1.getSig();
            auxl2 = l2.cabeza;
        }

        return interseccion;
    }

    public ListaS diferenciaSimetricaDeListas(ListaS l2) throws ExceptionUFPS {

        ListaS diferenciaSimetrica = new ListaS();
        Nodo<T> auxl1 = this.cabeza;
        Nodo<T> auxl2 = l2.cabeza;

        while (auxl1 != null) {
            while (auxl2 != null) {

                if (comparar(auxl2.getInfo(), auxl1.getInfo()) != 0 && diferenciaSimetrica.esta(auxl2.getInfo()) != true && this.esta(auxl2.getInfo()) != true) {
                    diferenciaSimetrica.insertarAlInicio(auxl2.getInfo());

                }

                if (comparar(auxl2.getInfo(), auxl1.getInfo()) != 0 && diferenciaSimetrica.esta(auxl1.getInfo()) != true && l2.esta(auxl1.getInfo()) != true) {
                    diferenciaSimetrica.insertarAlInicio(auxl1.getInfo());
                }
                auxl2 = auxl2.getSig();
            }
            auxl1 = auxl1.getSig();
            auxl2 = l2.cabeza;
        }
        return diferenciaSimetrica;
    }

    public ListaS diferenciaDeListas(ListaS l2) throws ExceptionUFPS {

        ListaS diferencia = new ListaS();
        ListaS interseccion = interseccionDeListas(l2);
        Nodo<T> auxl1 = this.cabeza;

        while (auxl1 != null) {

            if (!interseccion.esta(auxl1.getInfo()) && !diferencia.esta(auxl1.getInfo())) {
                diferencia.insertarAlInicio(auxl1.getInfo());
            }
            auxl1 = auxl1.getSig();
        }
        
        return diferencia;
    }
    
    public String productoCartesianoDeListas(ListaS l2)throws ExceptionUFPS{
    
        String msg = "{";
        
        Nodo<T> auxl1 = this.cabeza;
        Nodo<T> auxl2 = l2.cabeza;
        
        while(auxl1 != null){
        
            while(auxl2 != null){
            
                msg += "("+auxl1.getInfo()+","+auxl2.getInfo()+"), ";
                auxl2 = auxl2.getSig();
            }
            auxl1 = auxl1.getSig();
            auxl2 = l2.cabeza;
        }
        
        msg += "}";
        return msg;
    }

    public void eliminarRepetidos() throws ExceptionUFPS { // No pregunte como funciona, solo sepa que funciona.

        Nodo<T> aux1 = this.cabeza;
        Nodo<T> aux2 = aux1.getSig();
        while (aux2 != null) {
            while (aux2 != null) {
                if (comparar(aux1.getInfo(), aux2.getInfo()) == 0) {
                    eliminar(getIndice(aux2.getInfo()));
                    aux1 = this.cabeza;
                    aux2 = aux1.getSig();
                } else {
                    aux2 = aux2.getSig();
                }
            }
            aux1 = aux1.getSig();
            aux2 = aux1.getSig();
        }
    }

}//Fin de la Clase ListaS
